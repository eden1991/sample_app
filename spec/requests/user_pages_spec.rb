require 'spec_helper'

describe "User Pages" do

	subject { page }

	describe "profile page" do
		let(:user) { FactoryGirl.create(:user) }
		before { visit user_path(user) }

		it { should have_content(user.name) }
		it { should have_title(user.name) }
	end

	describe "signup page" do
		before { visit signup_path }

		it { should have_content('Sign up') }
		it { should have_title(full_title('Sign up')) }
	end


  describe "signup" do

    before { visit signup_path }

    let(:submit) { "Create my account" }

    describe "with invalid information" do
      before do
        fill_in "Name",         with: ""
        fill_in "Email",        with: ""
        fill_in "Password",     with: ""
        fill_in "Confirmation", with: ""
      end

      it "should have all blank fields" do
        expect(find_field('Name').value).to eq("")
        expect(find_field('Email').value).to eq("")
        expect(find_field('Password').value).to eq("")
        expect(find_field('Confirmation').value).to eq("")
      end

      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end

      it "Password field should not equal the Confirmation field" do
        expect(find_field('Password').value).to eq(find_field('Confirmation').value)
        puts(find_field('Password').value)
      end

      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
        fill_in "Name",         with: "Example User"
        fill_in "Email",        with: "user@example.com"
        fill_in "Password",     with: "foobar"
        fill_in "Confirmation", with: "foobar"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end
    end
  end
end
